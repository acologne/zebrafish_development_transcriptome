##################################
##### Create IR data #####
##################################
library(metaMA)
varSep<-read.table("~/Desktop/PostDoc/Logiciels/zebrafishdevelopmentshinyinterface/rawFiles/summarySNP.all.tab",h=T,sep="\t")
sCol=colnames(varSep)[grep("^s\\d+_\\d",colnames(varSep))]
varSep=varSep[rowSums(!is.na(varSep[,sCol]))!=0,]
varSep[,sCol]=round(varSep[,sCol],0)
varSep=varSep[,c("ID","pos","type","Ref","Alt","gene","bloc","localisation","transcript_consequence",sCol)]
varSep$varAllSamples=round(metaMA::rowVars(varSep[,sCol],na.rm=T),1)
varSep$varSelectedSamples=round(metaMA::rowVars(varSep[,sCol],na.rm=T),1)

varMerged=varSep[,c(1:9)]
for(i in c(1:18)) {
  name=paste("s",i,sep="")
  fetch=paste(name,"_\\d",sep="")
  sCol=colnames(varSep)[grep(fetch,colnames(varSep))]
  varMerged[name]=round(rowMeans(varSep[,sCol]),0)
}
sCol=colnames(varMerged)[grep("^s\\d+",colnames(varMerged))]
varMerged$varAllSamples=round(metaMA::rowVars(varMerged[,sCol],na.rm=T),1)
varMerged$varSelectedSamples=round(metaMA::rowVars(varMerged[,sCol],na.rm=T),1)
rm(i)
rm(fetch)
rm(name)
rm(sCol)

varSep=varSep[order(varSep$gene),]
varMerged=varMerged[order(varMerged$gene),]
save.image(file = "~/Desktop/PostDoc/Logiciels/zebrafishdevelopmentshinyinterface/Data/Var_results.RData")
