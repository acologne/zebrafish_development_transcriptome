# Module UI function
tablePanelVarUI <- function(id) {
  # Create a namespace function using the provided id
  ns <- NS(id)

  tagList(
      br(),
      DT::dataTableOutput(ns('data')),
      column(width = 12,
             br(),
             
             column(width = 6,
                    wellPanel(
                        h4("Download the csv file containing only the filtered events:"),
                        checkboxInput(ns("header"), "Header", TRUE),
                        checkboxInput(ns("quotes"), "With quotes", F),
                        radioButtons(ns("separator"),
                                     "Choose field separator",
                                     choiceNames = c("coma", "tab"),
                                     choiceValues = c(",", "\t"),
                                     inline = TRUE),
                        downloadButton(ns("downloadDataFiltered"), "Download the printed Dataset"),
                        downloadButton(ns("downloadData"), "Download the full Dataset")
                    )
             )
        )
    )
}

# Module server function
tablePanelVar <- function(input, output, session, nameData, dataInput) {
    
    # Keep only the selected columns
    dataToShow <- reactive({
      dataInput()
    })
    
    
    output$data <- DT::renderDataTable(
      if(nameData%in%c("varSep","varMerged")) {
        DT::datatable(dataToShow(),
                      rownames = FALSE, 
                      filter = "top",
                      extensions = 'Buttons', 
                      options = list(
                        dom = 'Bfrtip',
                        buttons = 
                          list('copy', 'print'),
                        pageLength = 30,
                        scrollX = TRUE
                      )
        )
      }
    )
    
    # download csv of the filtered dataset
    output$downloadData <- downloadHandler(
        filename = function() {
            if (input$separator == ","){
                return(paste(nameData, "_full.csv", sep = ""))
            } else if (input$separator == "\t"){
                return(paste(nameData, "_full.tsv", sep = ""))
            }
        },
        content = function(file) {
            write.table(dataToShow(), file, sep = input$separator,
                        col.names= input$header, row.names = FALSE, quote = input$quotes)
        }
    )
    
    output$downloadDataFiltered <- downloadHandler(
      filename = function() {
        if (input$separator == ","){
          return(paste(nameData, "_filtered.csv", sep = ""))
        } else if (input$separator == "\t"){
          return(paste(nameData, "_filtered.tsv", sep = ""))
        }
      },
      content = function(file) {
        write.table(dataToShow()[input[["data_rows_all"]],], file, sep = input$separator,
                    col.names= input$header, row.names = FALSE, quote = input$quotes)
      }
    )
    
}