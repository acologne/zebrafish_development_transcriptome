trackKSUI <- function(id) {
  ns <- NS(id)
  
  tagList(
    br(),
    fluidRow(
      column(width=12,
             br(),
             DT::dataTableOutput(ns('selectedPointsTrackKS'))
      )
    ),
    br(),
    sidebarLayout(position = "right",
      sidebarPanel(width = 3,
        h3("Filters"),
        checkboxGroupInput("trackFilterEvent","Filter this type of event:",choices = c("-","altA","altD","altAD","ES","ES_altA","ES_altD","ES_altAD","ES_MULTI","ES_MULTI_altA","ES_MULTI_altD","ES_MULTI_altAD","deletion","insertion","indel"),selected = c("deletion","insertion","indel")),
        numericInput("trackFilterMeanKS","Maximum mean PSI value for an event across all stages:",value=100,min=0,step=1),
        numericInput("trackFilterMaxKS","Maximum max PSI value for an event in at least one stage:",value=100,min=0,step=1),
        checkboxInput("trackFilterMatKS","Should the previous filters use data from the maternal stages (3 and 4)?",value = T),
        checkboxGroupInput("trackFilterStagesKS","Hide x-axis corresponding to stage:",choices = c(1:18)),
        
        h3("Plot aspect"),
        checkboxInput("addSegmentKS","Plot a line linking the same intron accross development",value = T),
        checkboxInput("reRangeKS","Should we convert PSIs in the 50-100 range to the 50-0 range?",value = F),
        checkboxInput("trackAspectU12geneKS","Should we use only Minor-Intron Containing genes?",value = F),
        checkboxInput("trackAspectU12onlyKS","Should we use only Minor Introns?",value = F),
        checkboxInput("trackAspectSepU12U2KS","Should we separate U12 and U2 genes?",value = T),

        h3("Individual plots"),
        checkboxInput("trackAspectIndivKS","Plot genes individually? (max. 30 events advised)",value = F),
        checkboxInput("trackScaleYKS","Use the same y-axis scale",value = T)
        
        
      ),
      mainPanel(withSpinner(plotOutput(ns('trackKS'),
                                       click = ns("plotClickTrackKS"),
                                       brush = brushOpts(
                                         id = ns("plotBrushTrackKS")
                                       )
      )))
    )
    
    
  )
  
}

trackPanelKS <- function(input, output, session, nameData, dataInput, indiv) {
  # compute the plot
  computePlot <- reactive({
    dataInput()
  })
  
  h <- reactive({
    updateH <- indiv()
    if(updateH) {
      data <- computePlot()
      return(300+as.integer((length(unique(data[[2]]$gene))-2)/2)*150)
    }
    return(850)
    })

  # plot
  output$trackKS <- renderPlot({
    # prepare data to plot
    data <- computePlot()
    #output$PCAgenesID <- row.names(dataPCA[[2]])
    return(data[[1]])
  },
  width = 850,
  height = h)
  
  # display the table containing the data of the Plot
  output$selectedPointsTrackKS <- DT::renderDataTable({
    data <- computePlot()
    dataToShow <- data[[2]]
    dataToUse <- dataToShow
    if (!is.null(input$plotBrushTrackKS)){
      dataToShowBrush <- brushedPoints(dataToUse, input$plotBrushTrackKS,xvar="x",yvar="y")
      dataToShow <- dataToShowBrush
    } else if (!is.null(input$plotClickTrackKS)){
      dataToShowClick <- nearPoints(dataToUse, input$plotClickTrackKS,xvar="x",yvar="y")
      dataToShow <- dataToShowClick
    }
    colnames(dataToShow) <- c("Stage", "PSI", "geneName","geneID","U12intron","U12gene")
    dataToShow$ID <- gsub("(.*)@.*@.*@.*@.*", "\\1", dataToShow$geneName)
    dataToShow$pos <- gsub(".*@.*@.*@(.*)@.*", "\\1", dataToShow$geneName)
    dataToShow$event <- gsub(".*@.*@.*@.*@(.*)", "\\1", dataToShow$geneName)
    dataToShow$geneName <- gsub(".*@.*@(.*)@.*@.*","\\1", dataToShow$geneName)
    dataToShow$U12gene <- dataToShow$U12gene=="U12 gene"
    dataToShow$U12intron <- dataToShow$U12intron=="U12"
    dataToShow=dataToShow[,c("Stage", "PSI", "ID", "geneName","geneID","pos", "event", "U12intron","U12gene")]
    DT::datatable(dataToShow, rownames = FALSE,
                  options = list(scrollX = TRUE),
                  caption = 'Select points on the plot to show them in this table.')
  })
}

