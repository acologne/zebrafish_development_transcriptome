# Module UI function
pcaPlotPanelKSUI <- function(id) {
  # Create a namespace function using the provided id
  ns <- NS(id)

  tagList(
      br(),
      fluidRow(
          column(width = 12,
                 withSpinner(plotOutput(ns('PCAKS'),
                                        click = ns("plotClickPCAKS"),
                                        brush = brushOpts(
                                            id = ns("plotBrushPCAKS")
                                        )
                            )
                 )
          )
      ),
      hr(),
      hr(),
      br(),
      br(),
      fluidRow(
          column(width = 12,
                 br(),
                 DT::dataTableOutput(ns('selectedPointsPCAKS'))
          )
      ),
      fluidRow(
        column(width=12,
               br(),
               DT::dataTableOutput(ns('PCAgenesIDKS'))
        )
      ),
      column(width = 12,
             br(),
             
             column(width = 6,
                    wellPanel(
                      h4("Download the csv file containing genes used for the PCA:"),
                      checkboxInput(ns("header"), "Header", TRUE),
                      checkboxInput(ns("quotes"), "With quotes", F),
                      radioButtons(ns("separator"),
                                   "Choose field separator",
                                   choiceNames = c("coma", "tab"),
                                   choiceValues = c(",", "\t"),
                                   inline = TRUE),
                      downloadButton(ns("downloadDataPCAKS"), "Download the table")
                    )
             )
      )
  )
}


# Module server function
pcaPlotPanelKS<- function(input, output, session, typeData, dataInput, labelPlot,
                        getParam){
  

    # compute the PCA
    computePCAKS <- reactive({
      dataInput()
    })
    
    # plot PCA
    output$PCAKS <- renderPlot({
        # prepare data to plot
        dataPCAKS <- computePCAKS()
        return(dataPCAKS[[1]])
    },
    width = 900,
    height = 500)
    
    output$PCAgenesIDKS <- DT::renderDataTable({
      dataToShow <- computePCAKS()[[2]]
      DT::datatable(dataToShow, rownames = FALSE,
                    options = list(scrollX = TRUE
                                   ),
                    caption = 'Splicing events used to draw the PCA (positionned by most to least variable).')
    })
    
    # display the table containing the data of the PCA
    output$selectedPointsPCAKS <- DT::renderDataTable({
        dataPCA <- computePCAKS()
        dataToShow <- dataPCA[[3]]
        dataToUse <- dataToShow
        if (!is.null(input$plotBrushPCAKS)){
            dataToShowBrush <- brushedPoints(dataToUse, input$plotBrushPCAKS)
            dataToShow <- dataToShowBrush
        } else if (!is.null(input$plotClickPCAKS)){
            dataToShowClick <- nearPoints(dataToUse, input$plotClickPCAKS)
            dataToShow <- dataToShowClick
        }
        DT::datatable(dataToShow, rownames = FALSE,
                      options = list(scrollX = TRUE),
                      caption = 'Select points on the plot to show them in this table.')
    })
    
    # download csv of the filtered dataset
    output$downloadDataPCAKS <- downloadHandler(
      filename = function() {
        if (input$separator == ","){
          return("PCA_events.csv")
        } else if (input$separator == "\t"){
          return("PCA_events.tsv")
        }
      },
      content = function(file) {
        write.table(computePCAKS()[[2]], file, sep = input$separator,
                    col.names= input$header, row.names = FALSE, quote = input$quotes)
      }
    )
}
