return(
  verticalLayout(
    sidebarLayout(position = "right",
                  sidebarPanel(h2("Parameters"),
                               width = 3,
                               
                               h3("Delta PSI value"),
                               checkboxInput("dPSIabsKS", "Should we take the absolute value of the deltaPSI?", T),
                               numericInput("dPSIKS",
                                            label = "Minimum (if positive) or maximum (of negative) dPSI value (PSI_group2 - PSI_group1) of an ASE:",
                                            min = -100,
                                            max = 100,
                                            value = 0),
                               
                               h3("Stages selection"),
                               selectizeInput(inputId = "dPSI1KS",label="Select one or more stages (first group for dPSI computation)",choices = colnames(KSMerged)[grep("stage.*",colnames(KSMerged))],selected=NULL,multiple=T),
                               selectizeInput(inputId = "dPSI2KS",label="Select one or more stages (second group for dPSI computation)",choices = colnames(KSMerged)[grep("stage.*",colnames(KSMerged))],selected=NULL,multiple=T),
                               numericInput("dPSInSampleKS",
                                            label = "Minimum number of sample required for the meanPSI estimation of each group:",
                                            min = 1,
                                            max = 5,
                                            value = 5),
                               
                               h3("Event filter"),
                               selectizeInput(inputId = "dPSIeventKS",label="Select one or more event type to filter",choices = unique(KSMerged$eventType),selected=c("insertion","deletion","indel"),multiple=T),
                               
                  ),
                  mainPanel(width=9,
                            actionButton("aDPSIKS", "Find events"),
                            dPSIKSPlotPanelUI("dPSIKSPlot")
                            
                  )
    )
  )
  
)