return(
  verticalLayout(
    fluidRow(column(width = 12,
                    radioButtons("clusterDataset","Chose the genes to track:",
                                 choiceNames=c("Cluster genes from the table above","Cluster a personnal set of genes","Cluster genes in GO term"),
                                 choiceValues = c(1,2,3),
                                 selected = 1),
                    conditionalPanel("input.clusterDataset==2",
                                     selectizeInput("clusterGene","Enter gene ID or gene name",choices = NULL,selected=NULL,multiple=T,options = list(create = T,maxOptions=0,placeholder = "ENSDARG00000000001 OR tmem")),
                    verbatimTextOutput("clusterSelectedGenes",placeholder = T)),
                    conditionalPanel("input.clusterDataset==3",
                                     selectizeInput("clusterGO","Enter a GO term or a description",choices = NULL,selected=NULL,multiple=T,options = list(create = T,maxOptions=0,placeholder = "GO:0097730 OR cilium")),
                                     verbatimTextOutput("clusterSelectedGO",placeholder = T)
                    ),
                    )),
    fluidRow(column(width=4,
                    actionButton("aCluster","Launch clustering"))),
    sidebarLayout(position = "right",
                  sidebarPanel(width = 3,
                    h3("Method"),
                    radioButtons("clusterMethod","Chose the clustering method:",choices = c("spearman","pearson","kendall"),selected = "spearman"),
                    numericInput("clusterN","Wanted number of cluster",value=5,min=1,step=1),
                    h3("Filters"),
                    checkboxInput("clusterU12only","Cluster only MIG genes?"),
                    checkboxInput("clusterSep","Cluster both MIG and non-MIG genes but separate them in the graphs",value = T),
                    numericInput("clusterFilterMean","Minimum mean TPM value for a gene across all stages:",value=0,min=0,step=1),
                    numericInput("clusterFilterMax","Minimum max TPM value for a gene for at least one stage:",value=0,min=0,step=1),
                    checkboxInput("clusterFilterMat","Should the previous filters use data from the maternal stages (3 and 4)?",value = F),
                    numericInput("clusterSet0","Set TPM values below this treshold to 0",value=0,min=0,step=1)
                  ),
                  mainPanel(width = 9,
                            withSpinner(
                              clusterPanelUI('showCluster'
                              )))
    )
  )
  
)