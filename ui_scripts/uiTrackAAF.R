return(
  navlistPanel(widths = c(2,10),
               tabPanel("Individual",
                        h3("Track a single variant"),
                        fluidRow(
                          column(width = 10,
                                 checkboxInput("exactMatchVar","Should we search for an exact match? (gene ID or gene Name)",value = F),
                                 selectizeInput("trackSingleVars","Enter rsID, gene ID or gene name (max. 30 advised)",choices = NULL,selected=NULL,multiple=T,options = list(create = T,maxOptions=0,maxItems=10,placeholder = "rs514787488, ENSDARG00000000001 or tmem")),
                                 verbatimTextOutput("trackSingleSelectedVar",placeholder = T)
                          ),
                          column(width=4,
                                 checkboxInput("trackSingleScaleYVar","Use the same y-axis scale",value = T),
                                 numericInput("trackSingleNColVar","Number of columns to display the graph:",value=2,min=1,max=5,step=1))
                        ),
                        
                        actionButton("aTrackSingleVar", "Plot AAF profile"),
                        withSpinner(trackSingleVarUI('showTrackSingleVar'))
               ),
               tabPanel("Multiple",
                        h3("Track multiple variants (the biological replicates will be merged)"),
                        radioButtons("trackDatasetVar","Chose the genes to track:",choiceNames=c("Track variants from the table above","Track personnal set of variants","Track variants from genes in selected biological processes"),choiceValues = c(1,2,3),selected = 1),
                        conditionalPanel("input.trackDatasetVar==2",
                                         checkboxInput("exactMatchVar2","Should we search for an exact match? (gene ID or gene Name)",value = F),
                                         selectizeInput("trackVar","Enter rsID, gene ID or gene name",choices = NULL,selected=NULL,multiple=T,options = list(create = T,maxOptions=0,placeholder = "rs514787488, ENSDARG00000000001 or tmem")),
                                         verbatimTextOutput("trackSelectedVar",placeholder = T)
                        ),
                        conditionalPanel("input.trackDatasetVar==3",
                                         selectizeInput("trackGOVar","Enter a GO term or a description",choices = NULL,selected=NULL,multiple=T,options = list(create = T,maxOptions=0,placeholder = "GO:0097730 OR cilium")),
                                         verbatimTextOutput("trackSelectedGOVar",placeholder = T)
                        ),
                        actionButton("aTrackVar", "Plot AAF profile"),
                        withSpinner(trackVarUI('showTrackVar'))
               )
  )
)