return(
  verticalLayout(
    sidebarLayout(position = "right",
                  sidebarPanel(h2("Parameters"),
                               width = 3,
                               
                               h3("Stages selection"),
                               selectizeInput(inputId = "marqWanted",label="Select one or more stages where the genes must be expressed (expressed stages)",choices = colnames(expMerged)[grep("stage.*",colnames(expMerged))],selected=NULL,multiple=T),
                               selectizeInput(inputId = "marqUnwanted",label="Select one or more stages where the genes must not be expressed (silent stages)",choices = colnames(expMerged)[grep("stage.*",colnames(expMerged))],selected=NULL,multiple=T,
                                              options = list(placeholder="All other stages")),
                               
                               h3("Cutoff"),
                               numericInput("marqMax","Minimum TPM value in the expressed stages",min=1, step=1, value = 6),
                               numericInput("marqMeanMax","Minimum mean TPM value in the expressed stages",min=1, step=1, value = 10),
                               numericInput("marqMin","Maximum TPM value in the silent stages",min=0, step=1, value = 3),
                               numericInput("marqMeanMin","Maximum mean TPM value in the silent stages",min=0, step=1, value = 1),
                               radioButtons("marqMIG",
                                            label = "Select genes containing a minor intron (U12 genes) or not (U2 genes):",
                                            choiceNames = c("U12 genes", "U2 genes", "both"),
                                            choiceValues = c("MIG", "non-MIG", "both"),
                                            selected = "both",
                                            inline = TRUE)
                               
                  ),
                  mainPanel(width=9,
                            actionButton("aMarq", "Find genes"),
                            marqPlotPanelUI("marqPlot")
                            
                  )
    )
  )
  
)