return(
  verticalLayout(
    sidebarLayout(position = "right",
                  sidebarPanel(h2("Parameters"),
                               width = 3,
                               
                               h3("Number of most variable introns to include"),
                               radioButtons("radPCAIR","Intron selection for PCA:",
                                            c("Use all the introns" = "all",
                                              "Use the variance to select the n most variable introns" = "var")),
                               numericInput("nIntronsPCA","Number of introns",min=2, step=1, value = 500),
                               #checkboxInput("allPCAIR", "Use all the introns (data will be standardize to have a mean and variance value of 0 and 1)", F),
                               
                               
                               h3("PC to plot"),
                               numericInput("PC1IR","x-axis PC",min=1, step=1, value = 1),
                               numericInput("PC2IR","y-axis PC",min=1, step=1, value = 2),
                               
                               h3("Axis"),
                               checkboxInput("invertXIR", "Invert x-axis", F),
                               checkboxInput("invertYIR", "Invert y-axis", F),
                               
                               h3("Colors"),
                               selectInput("PCAcolIR","Select the color palette to use:",choices=rownames(RColorBrewer::brewer.pal.info[RColorBrewer::brewer.pal.info$maxcolors>=11,]),selected = "Spectral")
                  ),
                  mainPanel(width=9,
                            actionButton("aPCAIR", "Update the PCA"),
                            pcaPlotPanelIRUI("plotPCAIR")
                            
                  )
    ),
    fluidRow(column(width=12,
                    h3("Introns contributing to each PC"),
                    pcaContribPanelIRUI("plotPCAcontribIR")))
  )
  
)