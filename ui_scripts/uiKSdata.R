return(
  # Sidebar with the different filters
  sidebarLayout(position = "right",
                sidebarPanel(h2("Filters"),
                             width = 3,
                             
                             h3("Events"),
                             checkboxInput("fCompleteCasesKS", "Only show complete cases (filter lines with one or more NA value)", F),
                             checkboxInput("add0KS", "Replace NA values with 0", F),
                             radioButtons("filterMIGKS",
                                          label = "Select events with a U12 introns (intron U12), in a gene containing a U12 intron (gene U12), in a gene without U12 introns (gene U2) or all events:",
                                          choiceNames = c("intron U12", "gene U12", "gene U2", "all"),
                                          choiceValues = c("U12", "MIG", "non-MIG", "all"),
                                          selected = "all",
                                          inline = TRUE),
                              
                             h3("Filter Developmental stages"),
                             
                             radioButtons("fKSAll",
                                          label = NULL,
                                          choiceNames = c("Select all", "Filter all"),
                                          choiceValues = c(F,T),
                                          inline = TRUE),
                             checkboxInput("fExp1KS", "Stage 1 (1-cell, 0hpf)", F),
                             checkboxInput("fExp2KS", "Stage 2 (2-cell, 0.75hpf)", F),
                             checkboxInput("fExp3KS", "Stage 3 (128-cell, 2.25hpf)", F),
                             checkboxInput("fExp4KS", "Stage 4 (1k-cell, 3hpf)", F),
                             checkboxInput("fExp5KS", "Stage 5 (Dome, 4.3hpf)", F),
                             checkboxInput("fExp6KS", "Stage 6 (50%epi, 5.25hpf)", F),
                             checkboxInput("fExp7KS", "Stage 7 (Shield, 6hpf)", F),
                             checkboxInput("fExp8KS", "Stage 8 (75%epi, 8hpf)", F),
                             checkboxInput("fExp9KS", "Stage 9 (1-4Som, 10.3hpf)", F),
                             checkboxInput("fExp10KS", "Stage 10 (14-19Som, 16hpf)", F),
                             checkboxInput("fExp11KS", "Stage 11 (20-25Som, 19hpf)", F),
                             checkboxInput("fExp12KS", "Stage 12 (Prim-5, 24hpf)", F),
                             checkboxInput("fExp13KS", "Stage 13 (Prim-15, 30hpf)", F),
                             checkboxInput("fExp14KS", "Stage 14 (Prim-25, 36hpf)", F),
                             checkboxInput("fExp15KS", "Stage 15 (Long-Pec, 48hpf)", F),
                             checkboxInput("fExp16KS", "Stage 16 (ProtrudingMouth, 3dpf)", F),
                             checkboxInput("fExp17KS", "Stage 17 (Day4, 4dpf)", F),
                             checkboxInput("fExp18KS", "Stage 18 (Day5, 5pf)", F),
                             
                             checkboxGroupInput("fEvents","Filter this type of event:",choices = c("-","altA","altD","altAD","ES","ES_altA","ES_altD","ES_altAD","ES_MULTI","ES_MULTI_altA","ES_MULTI_altD","ES_MULTI_altAD","deletion","insertion","indel"),selected = c("deletion","insertion","indel")),
                             checkboxGroupInput("fRepeats","Filter this type of repeat-linked event:",choices = c("exact","inexact","other")),
                             
                             h3("ERCC"),
                             checkboxInput("fERCCKS", "Include ERCC (spike-in) data", F),
                             
                             h3("Mean values"),
                             
                             checkboxInput("fAllKS", "Print the mean and variance on all samples", F),
                             checkboxInput("fMeanAllKS", "Apply/Remove filter on all mean values", TRUE),
                             conditionalPanel("input.fMeanAllKS",
                                              numericInput("nMeanAllminKS",
                                                           label = "Minimum mean value (all samples):",
                                                           min = 0,
                                                           max = 100,
                                                           value = 0),
                                              numericInput("nMeanAllmaxKS",
                                                           label = "Maximum mean value (all samples):",
                                                           min = 0,
                                                           max = 100,
                                                           value = 100)
                             ),
                             checkboxInput("fSelectedKS", "Print the mean and variance on selected samples", F),
                             checkboxInput("fMeanSelectedKS", "Apply/Remove filter on selected mean values", TRUE),
                             conditionalPanel("input.fMeanSelectedKS",
                                              numericInput("nMeanSelectedminKS",
                                                           label = "Minimum mean value (selected samples):",
                                                           min = 0,
                                                           max = 100,
                                                           value = 0),
                                              numericInput("nMeanSelectedmaxKS",
                                                           label = "Maximum mean value (selected samples):",
                                                           min = 0,
                                                           max = 100,
                                                           value = 100)
                             )
                             
                ),
                mainPanel(width = 9,
                          tabsetPanel(id = "tabKS",
                                      tabPanel('Merged biological replicates',
                                               withSpinner(
                                                 tablePanelKSUI('showDatasetKSMerged'
                                                 )
                                               )
                                      ),
                                      tabPanel('All samples',
                                               withSpinner(
                                                 tablePanelKSUI('showDatasetKS'
                                                 )
                                               )
                                      ),
                                      tabPanel('Information on exact-repeat events',
                                               withSpinner(
                                                 tablePanelKSrepeatUI('showDatasetKSe'
                                                 )
                                               )
                                      ),
                                      tabPanel('Information on inexact-repeat events',
                                               withSpinner(
                                                 tablePanelKSrepeatUI('showDatasetKSi'
                                                 )
                                               )
                                      ),
                                      tabPanel('Information on other-repeat events',
                                               withSpinner(
                                                 tablePanelKSrepeatUI('showDatasetKSo'
                                                 )
                                               )
                                      )
                          )
                )
  )
)
