library(shiny)

ui <- fluidPage(
  title = "Zebrafish Dataset",
  navbarPage("",
           # Welcome page
           tabPanel("Zebrafish Dataset",
                    fluidRow(
                      column(width=8, offset = 2,
                             div(h1("Welcome to the Shiny Application for \"Zebrafish Transcriptome during Development\""), br(),
                                 div(style= "text-align:justify", p("In most eukaryotes, two types of introns co-exist, major introns and minor introns, hence called because the first type largely outnumbers the second one. The major and minor class introns have distinct consensus splice site and branch point sequences and are removed by different spliceosomes, designed \"U2-type\" for the major and \"U12-type\" for the minor spliceosomes. Major and minor introns are also called U2- and U12-type introns."),
                                     p("Minor intron splicing plays a central role in human embryonic development and survival. Indeed, biallelic mutations in RNU4ATAC, the gene transcribed into the minor spliceosomal U4atac snRNA, are responsible for a rare autosomal recessive disorder named microcephalic osteodysplastic primordial dwarfism type 1 (MOPD1) or Taybi-Linder syndrome (TALS). RNU4ATAC is also involved in different and less severe developmental diseases, Roifman (RFMN) and Lowry-Wood (LWS) syndromes."),
                                     p("To gain knowledge on gene expression and splicing during development, taking into account whether they contain a minor intron or not, we used the publically available E-ERAD-475 study (ArrayExpress collection) containing 360 RNA-seq datasets from whole embryo mRNA at 18 developmental stages from 1 cell (0 hpf) to 5 days. The associated publication can be found ",
                                       a("here.", href="https://elifesciences.org/articles/30860", target="_blank"),"."),
                                     p("For each stage, we pooled the four technical replicates but kept the five biological replicates separated. We analysed these datasets with a special focus on minor introns and on the genes that contain them. However, this Shiny App can be used to explore the expression/splicing profile of any transcript!"),
                                     p("Reads from all stages were aligned with STAR to the GRCz11 genome version with ensembl95 annotation version. The same reads were also assembled using KisSplice."),
                                     p("Each section of this app focuses on one type of analysis: Expression, Intron Retention (IR) and Alternative Splicing (AS). Each section displays a dynamic table containing the raw results and gives access to several methods, PCA plot, differential analysis and track plot, which allow to follow one or more gene/intron/splicing event throughout the different stages. Below is a quick description of each of these sections and of each of the methods.")),
                                 br(), hr(), br(),
                                 align = "center"
                             ),
                             div(h1(strong("Expression Analysis")),br(),
                                 p("This section gives results related to gene expression measurement. The metric used is the TPM, Transcript Per Million, as computed by RSEM."),
                                 p("One line per gene. The columns give the TPM value of each gene at each developmental stage for each biological replicate (they can be merged). The last column indicates if the gene contains a minor intron (for those genes, the line is highlighted in yellow)."),
                                 br()),
                             div(h1(strong("Intron Retention (IR) Analysis")),br(),
                                 p("This section gives results related to intron retention detection and measurement. The metric used is the PSI, Percent Spliced In, as computed by KisSplice2refgenome and kissDE. PSI is comprised between 0% and 100%; the higher the value, the higher the retention."),
                                 p("One line per intron. The columns give the PSI value of each intron at each developmental stage for each biological replicate (they can be merged). The last columns indicate if the gene contains a minor intron and if the intron is a minor one (for those introns, the line is highlighted in yellow)."),
                                 br()),
                             div(h1(strong("Alternative Splicing (AS) Analysis")),br(),
                                 p("This section shows results related to alternative splicing event detection and measurement. The metric used is the PSI, Percent Spliced In, as computed by KisSplice2refgenome and kissDE. PSI is comprised between 0% and 100%; the higher the value, the higher the abundance of the longer isoform."),
                                 p("One line per alternative splicing event. The columns give the PSI value of each event at each developmental stage for each biological replicate (they can be merged). The columns indicate the type of event, if the event involves a U12-type donor or acceptor splice site, a minor intron and if it concerns a gene that contains a minor intron (for those introns, the line is highlighted in yellow)."),
                                 br()),
                             hr(),
                             br(),
                             div(h2(strong("Tracking graph analysis")),br(),
                                 p("Enables to select one or more genes/introns/splicing events, or genes involved in a given biological process, and visualize their expression or splicing profile throughout the selected stages."),
                                 br()),
                             div(h2(strong("PCA-plot analysis (using ade4 version 1.7-17)")),br(),
                                 p("Enables to perform PCA on genes/introns/events of the active table. Users can select all of them or choose the number of genes/introns/events to use. Which PC to plot on the 2D graph can also be selected."),
                                 p("Concerning gene expression, all or the n-most variable genes can be used. If all the genes are selected, then the TPM distribution is rescale to have a mean value of 0 and a variance of 1. Else, the n-most variable genes are selected, which will bias the PCA toward higly expressed genes as variability increase with expression."),
                                 h3("Genes/introns/events contributing to each PC"),
                                 p("In addition to the PCA-plot, the expression profile of the genes most contributing to each PC will also be shown, to give a visual overview of the main dynamics of the different developmental stages."),
                                 br()),
                             div(h2(strong("Differential analysis (using DESeq2 version 1.32.0 or kissDE version 1.15.0)")),br(),
                                 p("Enables to run differential analyses between any two groups of selected stages. These analyses are time-consuming for IR and AS."),
                                 p("In addition to providing a comprehensive summary table, an interactive summary plot will also be shown (MA-plot for expression level, PSI-plot for IR and AS)."),
                                 h3("GO enrichment analysis (using TopGO version 2.44.0)"),
                                 p("Enables to run a GO enrichment analysis on the differentially expressed/spliced genes using the genome wide annotation for zebrafish org.Dr.eg.db version 3.13.0. Depending on the number of differential elements analysed, this can be a time-consuming step."),
                                 p("Each gene associated with a GO term of interest can then be accessed with the GOID (that can be found in the TopGO result table on the Shiny App). Further information on these genes can be accessed in a table and a plot."),
                                 p("Users can also create a bar-plot graph to summarize the GO enrichment results."),
                                 br()),
                             column(width=12,
                                    div(img(src = "https://lbbe.univ-lyon1.fr/sites/default/files/icons/logo_1.svg", height = 75, hspace = 5), 
                                        img(src = "Lyon1.jpg", height = 70, hspace = 5), 
                                        img(src = "INRIA.jpg", height = 65, hspace = 5), 
                                        img(src = "CNRS.jpg", height = 65, hspace = 5), 
                                        img(src = "CRNL.png", height = 95, hspace = 5),
                                        align = "center"
                                    )
                             )
                      )
                    )
           ),
           
           tabPanel("Expression Analysis",
                    fluidRow(column(width=12,
                                    h1(strong("Expression Analysis"))),
                             fluidRow(column(width=12,
                                             h2("TPM tables"),
                                             uiOutput("uiExprData")
                             )),
                             fluidRow(column(width = 6,
                                             actionButton("aUpdateTable","Update the table with only the analyzed data")),
                                      column(width = 6,
                                             actionButton("aResetTable","Reset the table"))),
                             h2("Analyses:"),
                             tabsetPanel(id = "tabTreatment",
                                         tabPanel("Track Expression",
                                                  withSpinner(uiOutput("uiTrackExpr"))),
                                         tabPanel("PCA",
                                                  withSpinner(uiOutput("uiPCA"))),
                                         tabPanel("Find genes with stage-specific expression",
                                                  withSpinner(uiOutput("uiMarq"))),
                                         tabPanel("DESeq2",
                                                  h3("Select 2 groups of samples to compare:"),
                                                  withSpinner(uiOutput("uiDESQ2")))
                                         #tabPanel("Cluster by Expression",
                                          #        withSpinner(uiOutput("uiClusterExpr")))
                                         )
                             
                    )),
           
           tabPanel("Intron Retention (IR) Analysis",
                    fluidRow(column(width=12,
                                    h1(strong("IR Analysis"))),
                             fluidRow(column(width=12,
                                             h2("PSI tables"),
                                             uiOutput("uiIRdata")
                             )),
                             fluidRow(column(width = 6,
                                             actionButton("aUpdateTableIR","Update the table with only the analyzed data")),
                                      column(width = 6,
                                             actionButton("aResetTableIR","Reset the table"))),
                             h2("Analyses:"),
                             tabsetPanel(id = "tabTreatmentIR",
                                         tabPanel("Track PSI",
                                                  withSpinner(uiOutput("uiTrackPSI"))),
                                         tabPanel("PCA",
                                                  withSpinner(uiOutput("uiPCAIR"))),
                                         tabPanel("Differential IR",
                                                  withSpinner(uiOutput("uiDiffIR"))))
                             
                    )),
           
           tabPanel("Splicing Analysis with KisSplice",
             fluidRow(column(width=12,
                             h1(strong("Splicing Analysis"))),
                      fluidRow(column(width=12,
                                      h2("PSI tables"),
                                      uiOutput("uiKSdata")
                      )),
                      fluidRow(column(width = 6,
                                      actionButton("aUpdateTableKS","Update the table with only the analyzed data")),
                               column(width = 6,
                                      actionButton("aResetTableKS","Reset the table"))),
                      h2("Analyses:"),
                      tabsetPanel(id = "tabTreatmentKS",
                                  tabPanel("Track PSI",
                                           withSpinner(uiOutput("uiTrackPSIKS"))),
                                  tabPanel("PCA",
                                           withSpinner(uiOutput("uiPCAKS"))),
                                  tabPanel("Find events with specific dPSI",
                                           withSpinner(uiOutput("uiDPSIKS"))),
                                  tabPanel("Differential Splicing Events",
                                           withSpinner(uiOutput("uiDiffKS"))))
                      
             )),
           
           if(FALSE) {
             tabPanel("SNP/indels analysis",
                      fluidRow(column(width=12,
                                      h1(strong("SNP/indels Analysis"))),
                               fluidRow(column(width=12,
                                               h2("Alternative-Allele Frequency (AAF) tables"),
                                               uiOutput("uiVardata")
                               )),
                               fluidRow(column(width = 6,
                                               actionButton("aUpdateTableVar","Update the table with only the analyzed data")),
                                        column(width = 6,
                                               actionButton("aResetTableVar","Reset the table"))),
                               h2("Analyses:"),
                               tabsetPanel(id = "tabTreatmentVar",
                                           tabPanel("Track AAF",
                                                    withSpinner(uiOutput("uiTrackAAF"))),
                                           tabPanel("PCA",
                                                    withSpinner(uiOutput("uiPCAvar"))))
                               
                      ))
           }
           
           
)
)